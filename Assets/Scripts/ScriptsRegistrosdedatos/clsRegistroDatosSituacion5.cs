﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSInterfazAvanzada;
using NSSituacion5;
using NSTraduccionIdiomas;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class clsRegistroDatosSituacion5 : AbstractSingletonPanelUIAnimation<PanelInterfazRegistroDatos>
    {
        #region members

        [SerializeField] private ControladorSituacion5 refControladorSituacion5;

        [SerializeField] private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField] private PanelInterfazEvaluacion refPanelInterfazEvaluacion;

        [SerializeField] private ControladorValoresPDF refControladorValoresPDF;

        [SerializeField] private Button buttonReporte;

        [SerializeField] private Evaluacion refEvaluacion;

        [Header("Inputs baja tension")]
        [SerializeField] private TMP_InputField inputBajaTensionX1;
        
        [SerializeField] private TMP_InputField inputBajaTensionX2;
        
        [SerializeField] private TMP_InputField inputBajaTensionX3;
                
        [Header("Inputs baja corriente")]
        [SerializeField] private TMP_InputField inputBajaCorrienteX1;
        
        [SerializeField] private TMP_InputField inputBajaCorrienteX2;
        
        [SerializeField] private TMP_InputField inputBajaCorrienteX3;
                
        [Header("Inputs relacion transformacion")]
        [SerializeField] private TMP_InputField inputRelacionTransformacionX1;
        
        [SerializeField] private TMP_InputField inputRelacionTransformacionX2;
        
        [SerializeField] private TMP_InputField inputRelacionTransformacionX3;
                
        [Header("Inputs corriente")]
        [SerializeField] private TMP_InputField inputCorrienteX1;
        
        [SerializeField] private TMP_InputField inputCorrienteX2;
        
        [SerializeField] private TMP_InputField inputCorrienteX3;
                
        [Header("Error porcentual")]
        [SerializeField] private TMP_InputField inputErrorPorcentualX1;
        
        [SerializeField] private TMP_InputField inputErrorPorcentualX2;
        
        [SerializeField] private TMP_InputField inputErrorPorcentualX3;       
        
        /// <summary>
        /// Calificacion final  
        /// </summary>
        private float calificacion;
        
        private float fragmentoCalificacion;
        
        private float porcentajeError = 0.05f;

        #endregion

        public void OnButtonValidar()
        {
            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }

            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarFelicitaciones"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectos"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                refControladorDatosSesion.AddIntentos();
            }
        }

        public void OnButtonReporte()
        {
            if (ValidarEmptyInputs())
            {
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
                return;
            }
            
            if (ValidarDatos())
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarCorrectos"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
            else
            {
                BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeValidarIncorrectosReporte"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextReporte"), OnDatosCorrectos);
                refControladorDatosSesion.AddIntentos();
            }
        }

        private void OnDatosCorrectos()
        {
            Mostrar(false);
            refControladorValoresPDF.SetPanelRegistroDatos();
            refPanelInterfazEvaluacion.Mostrar();
        }

        
        
        private bool ValidarDatos()
        {
            ResetInputsIncorrectos();

            fragmentoCalificacion = 1f / 15f;
            calificacion = 0;

            VerificarValorInput(inputBajaTensionX1, refControladorSituacion5._tensionFasePuntasPinza[0]);
            VerificarValorInput(inputBajaTensionX2, refControladorSituacion5._tensionFasePuntasPinza[1]);
            VerificarValorInput(inputBajaTensionX3, refControladorSituacion5._tensionFasePuntasPinza[2]);
            
            VerificarValorInput(inputBajaCorrienteX1, refControladorSituacion5._corrienteCablesTransformador[0]);
            VerificarValorInput(inputBajaCorrienteX2, refControladorSituacion5._corrienteCablesTransformador[1]);
            VerificarValorInput(inputBajaCorrienteX3, refControladorSituacion5._corrienteCablesTransformador[2]);

            if (inputRelacionTransformacionX1.text == inputRelacionTransformacionX2.text && inputRelacionTransformacionX1.text == inputRelacionTransformacionX3.text)
            {
                VerificarValorInput(inputRelacionTransformacionX1, refControladorSituacion5.RelacionTransformacion);
                VerificarValorInput(inputRelacionTransformacionX2, refControladorSituacion5.RelacionTransformacion);
                VerificarValorInput(inputRelacionTransformacionX3, refControladorSituacion5.RelacionTransformacion);
            }
            else
            {
                SetInputIncorrecto(inputRelacionTransformacionX2);
                SetInputIncorrecto(inputRelacionTransformacionX3);
            }

            VerificarValorInput(inputCorrienteX1, refControladorSituacion5._corrienteCablesMacromedidor[0]);
            VerificarValorInput(inputCorrienteX2, refControladorSituacion5._corrienteCablesMacromedidor[1]);
            VerificarValorInput(inputCorrienteX3, refControladorSituacion5._corrienteCablesMacromedidor[2]);
            
            VerificarValorInput(inputErrorPorcentualX1, refControladorSituacion5._tensionFasePuntasPinza[0]);
            VerificarValorInput(inputErrorPorcentualX2, refControladorSituacion5._tensionFasePuntasPinza[1]);
            VerificarValorInput(inputErrorPorcentualX3, refControladorSituacion5._tensionFasePuntasPinza[2]);
            
            refEvaluacion.AsignarCalificacionRegistroDatos(calificacion);
            Debug.Log("AsignarCalificacionRegistroDatos : " + calificacion);

            if (Mathf.Approximately(calificacion, 1f))
                return true;

            return false;
        }

        private void VerificarValorInput(TMP_InputField argInputField, float argValorAComparar)
        {
            float tmpValorFloatDelInput;
            float.TryParse(argInputField.text, out tmpValorFloatDelInput);
            
            var tmpDiferenceValueIsCorrect = Mathf.Abs(tmpValorFloatDelInput - argValorAComparar) <= argValorAComparar * porcentajeError;

            if (tmpDiferenceValueIsCorrect)
                calificacion += fragmentoCalificacion;
            else
                SetInputIncorrecto(argInputField);
        }

        private float GetValorFloatInputField(TMP_InputField argInputField)
        {
            float tmpValorFloatDelInput;
            float.TryParse(argInputField.text, out tmpValorFloatDelInput);
            
            return tmpValorFloatDelInput;
        }

        private bool ValidarEmptyInputs()
        {
            var tmpEmptyInputs = 0;

            //Baja tension
            if (inputBajaTensionX1.text.Equals(""))
                tmpEmptyInputs++;

            if (inputBajaTensionX2.text.Equals(""))
                tmpEmptyInputs++;

            if (inputBajaTensionX3.text.Equals(""))
                tmpEmptyInputs++;

            
            if (inputBajaCorrienteX1.text.Equals(""))
                tmpEmptyInputs++;

            if (inputBajaCorrienteX2.text.Equals(""))
                tmpEmptyInputs++;

            if (inputBajaCorrienteX3.text.Equals(""))
                tmpEmptyInputs++;

            
            if (inputRelacionTransformacionX1.text.Equals(""))
                tmpEmptyInputs++;

            if (inputRelacionTransformacionX2.text.Equals(""))
                tmpEmptyInputs++;
            
            if (inputRelacionTransformacionX3.text.Equals(""))
                tmpEmptyInputs++;
            
            
            if (inputCorrienteX1.text.Equals(""))
                tmpEmptyInputs++;
            
            if (inputCorrienteX2.text.Equals(""))
                tmpEmptyInputs++;
            
            if (inputCorrienteX3.text.Equals(""))
                tmpEmptyInputs++;
            
            
            if (inputErrorPorcentualX1.text.Equals(""))
                tmpEmptyInputs++;
            
            if (inputErrorPorcentualX2.text.Equals(""))
                tmpEmptyInputs++;
            
            if (inputErrorPorcentualX3.text.Equals(""))
                tmpEmptyInputs++;
            
            return tmpEmptyInputs > 0;
        }

        private void ResetInputsIncorrectos()
        {
            SetInputIncorrecto(inputBajaTensionX1, false);
            SetInputIncorrecto(inputBajaTensionX2, false);
            SetInputIncorrecto(inputBajaTensionX3, false);
            
            SetInputIncorrecto(inputBajaCorrienteX1, false);
            SetInputIncorrecto(inputBajaCorrienteX2, false);
            SetInputIncorrecto(inputBajaCorrienteX3, false);
            
            SetInputIncorrecto(inputRelacionTransformacionX1, false);
            SetInputIncorrecto(inputRelacionTransformacionX2, false);
            SetInputIncorrecto(inputRelacionTransformacionX3, false);
            
            SetInputIncorrecto(inputCorrienteX1, false);
            SetInputIncorrecto(inputCorrienteX2, false);
            SetInputIncorrecto(inputCorrienteX3, false);

            SetInputIncorrecto(inputErrorPorcentualX1, false);
            SetInputIncorrecto(inputErrorPorcentualX2, false);
            SetInputIncorrecto(inputErrorPorcentualX3, false);
        }

        private void SetInputIncorrecto(TMP_InputField argInputField, bool argIncorrecto = true)
        {
            argInputField.transform.GetChild(1).gameObject.SetActive(argIncorrecto);
        }

        public bool ValidarCamposRelacionTransformacionLlenos()
        {
            if (inputRelacionTransformacionX1.text.Equals("") || inputRelacionTransformacionX2.text.Equals("") || inputRelacionTransformacionX3.text.Equals(""))
                return false;
            
            return true;
        }

        /// <summary>
        /// Cuando se termina de asignar la relacion de transformacion en los input field
        /// </summary>
        public void EscribioRelacionTransformacion()
        {
            var tmpValueRelacionTransformacion = GetValorFloatInputField(inputRelacionTransformacionX1) + GetValorFloatInputField(inputRelacionTransformacionX2) + GetValorFloatInputField(inputRelacionTransformacionX3);
            tmpValueRelacionTransformacion /= 3;
            refControladorSituacion5.RelacionTransformacionUsuario = tmpValueRelacionTransformacion;
        }
    }
}