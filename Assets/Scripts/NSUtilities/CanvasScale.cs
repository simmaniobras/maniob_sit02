using UnityEngine;
using NSAvancedUI;

namespace NSUtilities
{
    public class CanvasScale : MonoBehaviour
    {
        #region members

        [SerializeField] private Canvas canvasMain;

        public clsCuaderno cuaderno;

        private float canvasWidthFactor;
        
        private float canvasHeightFactor;

        public float CanvasWidthFactor
        {
            get { return canvasWidthFactor; }
        }

        public float CanvasHeightFactor
        {
            get { return canvasHeightFactor; }
        }
        
        #endregion

        #region accesors

        #endregion

        #region mono behaviour

        private void Awake()
        {
            var tmpCanvasPixelPerfect = canvasMain.pixelRect;
            canvasWidthFactor = tmpCanvasPixelPerfect.width / 1920f;
            canvasHeightFactor = tmpCanvasPixelPerfect.height / 1080f;
            cuaderno.InitCuaderno();
            cuaderno.mtdPasarInfoAlPdf();
        }

        #endregion

        #region private methods

        #endregion

        #region public methods

        #endregion

        #region courutines

        #endregion
    }
}