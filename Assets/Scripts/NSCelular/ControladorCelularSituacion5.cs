﻿using NSEvaluacion;
using System.Collections;
using NSSituacion5;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSCelular
{
    public class ControladorCelularSituacion5 : MonoBehaviour
    {

        #region members

        private int mensajesEnviados;

        private string circuitoID;

        private string transformadorID;

        private string apoyoID;

        [SerializeField] private ControladorConexionesMacroMedidor refControladorConexionesMacroMedidor;
        
        [SerializeField] private GameObject prefabMensajeUsuario;

        [SerializeField] private GameObject prefabMensajeCentroMando;

        [SerializeField] private Transform transformContentList;

        [SerializeField] private ScrollRect scrollRectCelular;

        [SerializeField] private TextMeshProUGUI textMensajeAEnviar;
    
        [SerializeField] private Evaluacion refEvaluacion;

        [SerializeField] private Button buttonEnviarMensaje;

        private float calificacionEnvioMensajes;
        #endregion

        private void Awake()
        {
            IniciarCelular();
        }

        private void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine(CouControladoraMensajes());
        }

        public void IniciarCelular()
        {
            circuitoID = "R" + Random.Range(0, 10000).ToString("X4");
            transformadorID = "T" + Random.Range(0, 10000).ToString("X4");
            apoyoID = "M" + Random.Range(0, 100000).ToString("X5");
        }

        public void OnButtonEnviarMensaje()
        {
            var tmpPrefabMensajeUsuario = Instantiate(prefabMensajeUsuario, transformContentList);
            tmpPrefabMensajeUsuario.GetComponent<SetMessage>().SetMensaje(textMensajeAEnviar.text);
            textMensajeAEnviar.text = "";
            LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
            scrollRectCelular.verticalNormalizedPosition = 0;
            mensajesEnviados++;
            Debug.Log("mensajesEnviados : "+ mensajesEnviados);
            buttonEnviarMensaje.interactable = false;
        }

        public void SetNuevoMensajeUsuario()
        {
            textMensajeAEnviar.text = GetMensajeParaEnviar();
            buttonEnviarMensaje.interactable = true;
        }

        public void SetMensaje1()
        {
            mensajesEnviados = 0;
            SetNuevoMensajeUsuario();
        }

        public void SetMensaje2()
        {
            mensajesEnviados = 2;
            SetNuevoMensajeUsuario();
        }

        public void SetMensaje3()
        {
            mensajesEnviados = 4;
            SetNuevoMensajeUsuario();
        }

        public void SetMensaje4()
        {
            mensajesEnviados = 6;
            SetNuevoMensajeUsuario();
        }

        private string GetMensajeParaEnviar()
        {
            var tmpMensaje = "";

            if (mensajesEnviados == 0)
                tmpMensaje = "Centro de control para Operario. Solicito la consignación PN0441X del circuito " + circuitoID + " para el cambio de lo descargadores de tensión DPS en el transformador " + transformadorID + " en el apoyo " + apoyoID + ".";
            else if (mensajesEnviados == 2)
                tmpMensaje = "Solicito la autorización para la apertura de cortacircuitos en del transformador " + transformadorID + " con el fin de dar inicio a la consigna PN0441X.";            
            else if (mensajesEnviados == 4)
                tmpMensaje = "Centro de control para operario. Las labores de montaje del macromedidor en el transformador " + transformadorID + " en el apoyo M04425 han sido finalizadas. Solicito autorización para conexión de cortacircuitos.";            
            else if (mensajesEnviados == 6)
                tmpMensaje = "Comunico que los cortacircuitos en el transformador " + transformadorID + " han sido conectados a la red.";

            return tmpMensaje;
        }

        private string GetMensajeCentroControl()
        {
            var tmpMensaje = "";

            if (mensajesEnviados == 1)
                tmpMensaje = "Confirmo la solicitud y el inicio de la consignación PN0441X del circuito " + circuitoID + ".";
            else if (mensajesEnviados == 3)
                tmpMensaje = "Confirmo la solicitud de apertura de los cortacircuitos en el apoyo " + transformadorID + " para la consigna PN0441X. Efectúe la maniobra.";
            else if (mensajesEnviados == 5)
            {
                if (refControladorConexionesMacroMedidor._macromedidorConectado)
                    tmpMensaje = "Confirmo la finalización de la actividad. Proceda a realizar el cierre de cortacircuitos.";
                else
                    tmpMensaje = "Revise la conexión de los elementos.";
            }
            else if (mensajesEnviados == 7)
                tmpMensaje = "Confirmo el cierre de los cortacircuitos y la finalización de la consigna PN0441X.";

            return tmpMensaje;
        }

        private IEnumerator CouControladoraMensajes()
        {
            while (true)
            {
                if (mensajesEnviados == 1 || mensajesEnviados == 3 || mensajesEnviados == 5 || mensajesEnviados == 7 || mensajesEnviados == 9)
                {
                    yield return new WaitForSeconds(Random.Range(1f, 2f));
                    var tmpPrefabMensajeCentroControl = Instantiate(prefabMensajeCentroMando, transformContentList);
                    tmpPrefabMensajeCentroControl.GetComponent<SetMessage>().SetMensaje(GetMensajeCentroControl());
                    LayoutRebuilder.ForceRebuildLayoutImmediate(transformContentList.GetComponent<RectTransform>());
                    calificacionEnvioMensajes += 1f / 5f;

                    if (mensajesEnviados == 9)
                        refEvaluacion.AsignarCalificacionMensajesEnviados(calificacionEnvioMensajes);

                    mensajesEnviados++;
                }

                yield return null;
                scrollRectCelular.verticalNormalizedPosition = 0;
            }
        }

        public void BorrarMensajes()
        {
            while (transformContentList.childCount > 0)
                DestroyImmediate(transformContentList.GetChild(0).gameObject);
        }
    }
}