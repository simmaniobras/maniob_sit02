using System;
using NSBoxMessage;
using NSInterfaz;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace NSSituacion5
{
    public class VentanaSeleccionCable : MonoBehaviour
    {
        #region members
        
        [SerializeField] private Toggle[] arrayToggleCableSelected;

        public UnityEventConexionCable OnFinishConexionCable;

        /// <summary>
        /// Para saber si selecciono el cable S1 o S2
        /// </summary>
        private int indexToggleCableSeleccionado = -1;

        /// <summary>
        /// Indice del color del cable, 0 = amarillo claro, 1 = amarillo oscuro, 2 = azul claro, 3 = azul oscuro, 4 = rojo claro, 5 = rojo oscuro, 6 = blanco
        /// </summary>
        private int indexCableSeleccionado;
        
        private PanelInterfazSoloAnimacion refPanelInterfazSoloAnimacion;

        private bool todosTogglesSeleccionados;
        #endregion
        
        #region mono behaviour

        private void Awake()
        {
            refPanelInterfazSoloAnimacion = GetComponent<PanelInterfazSoloAnimacion>();
        }

        #endregion
        
        #region public methods

        public void OnButtonAceptar()
        {
            if (indexToggleCableSeleccionado == -1)
            {
                todosTogglesSeleccionados = true;
                CheckTodosTogglesSeleccionados();
                
                if (todosTogglesSeleccionados)
                    refPanelInterfazSoloAnimacion.Mostrar(false);
                else
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeDebeSeleccionarCable"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
            }
            else
            {
                OnFinishConexionCable.Invoke(indexCableSeleccionado, arrayToggleCableSelected[indexToggleCableSeleccionado], indexToggleCableSeleccionado);
                
                foreach (var tmpToggle in arrayToggleCableSelected)
                    tmpToggle.isOn  = false;

                indexToggleCableSeleccionado = -1;
                indexCableSeleccionado = -1;
                refPanelInterfazSoloAnimacion.Mostrar(false);
            }
        }

        public void OnToggleSelected(int argIndexToggleCableSeleccionado)
        {
            indexToggleCableSeleccionado = argIndexToggleCableSeleccionado;//para los cables TC, puede seleccionar index 1 o 2, par los de voltaje solo 1
        }

        public void SetColorCableSeleccionado(int argIndexColorCable)
        {
            indexCableSeleccionado = argIndexColorCable;
        }
        #endregion

        #region private

        private void CheckTodosTogglesSeleccionados()
        {
            foreach (var tmpToggle in arrayToggleCableSelected)
                todosTogglesSeleccionados = todosTogglesSeleccionados && !tmpToggle.isOn;
        }

        #endregion

    }

    [Serializable]
    public class UnityEventConexionCable : UnityEvent<int, Toggle, int>
    {
        
    }
}