using System;
using UnityEngine;
using UnityEngine.UI;

namespace NSSituacion5
{
    public abstract class AbstractCable : MonoBehaviour
    {
        #region members

        protected Image imageCable;
        #endregion

        #region accesors
        
        public Color ColorCable
        {
            set
            {
                if (!imageCable)
                    imageCable = GetComponent<Image>();
                
                imageCable.color = value;
            }
            get { return imageCable.color; }
        }

        #endregion
        
        #region mono behaviour

        private void Awake()
        {
            imageCable = GetComponent<Image>();
        }
        #endregion

        #region public methods

        public void ActivarCable()
        {
            gameObject.SetActive(true);
        }

        public virtual void DesactivarCable()
        {
            gameObject.SetActive(false);
        }
        #endregion
    }
}