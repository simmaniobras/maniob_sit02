using UnityEngine;

namespace NSSituacion5
{
    public class CablePuente : AbstractCable
    {
        private int conexionFinal = -1;

        public int ConexionFinal
        {
            get { return conexionFinal; }
            set { conexionFinal = value; }
        }
        #region public methods

        public override void DesactivarCable()
        {
            base.DesactivarCable();
            ConexionFinal = -1;
        }
        #endregion
    }
}