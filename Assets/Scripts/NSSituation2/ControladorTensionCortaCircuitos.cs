﻿using NSPuntaDetectora;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSituacion2
{
    public class ControladorTensionCortaCircuitos : MonoBehaviour
    {
        [SerializeField]
        private PuntoMedicionPuntaDetectora[] arraysPuntosMedicionInferior;

        public void DesactivarTensionInferior()
        {
            foreach (var puntoMedicion in arraysPuntosMedicionInferior)
                puntoMedicion._tieneTension = false;
        }

        public void ActivarTensionInferior()
        {
            foreach (var puntoMedicion in arraysPuntosMedicionInferior)
                puntoMedicion._tieneTension = true;
        }
    } 
}
