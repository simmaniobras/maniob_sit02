using UnityEngine;

namespace NSSituation2
{
    public class ControladorArnes : MonoBehaviour
    {
        #region members

        [SerializeField] private GameObject arnes;
        
        [SerializeField] private GameObject arnesConectado;

        [SerializeField] private RectTransform rectTransformPivoteArnes;
        
        #endregion
        
        #region public methods

        public void ConectarArnes()
        {
            arnes.SetActive(false);
            arnesConectado.SetActive(true);
            rectTransformPivoteArnes.anchoredPosition = new Vector2(-99, -130);
        }
        #endregion
    }
}