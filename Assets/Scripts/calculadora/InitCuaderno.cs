﻿using System.Collections;
using UnityEngine;

namespace NSTest.calculadora
{
    public class InitCuaderno : MonoBehaviour
    {
        #region members

        [SerializeField] private clsCuaderno refCuaderno;

        #endregion

        #region MonoBehaviour

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(3);
            refCuaderno.InitCuaderno();
            refCuaderno.mtdPasarInfoAlPdf();
        }
        #endregion
    }
}