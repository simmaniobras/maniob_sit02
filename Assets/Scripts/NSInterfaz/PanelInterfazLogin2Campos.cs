﻿using NSAvancedUI;
using NSBoxMessage;
using NSSeguridad;
using NSTraduccionIdiomas;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfaz
{
    public class PanelInterfazLogin2Campos : AbstractPanelUIAnimation
    {
        #region members

        [SerializeField]
        private TMP_InputField inputUsuario;

        [SerializeField]
        private TMP_InputField inputPassword;
        
        [SerializeField]
        private TextMeshProUGUI textUsuarioPlaceHolder;

        [SerializeField]
        private TextMeshProUGUI textPasswordPlaceHolder;

        [SerializeField]
        //private GameObject interfazAfuera;

        /// <summary>
        /// texto boton iniciar
        /// </summary>
        private TextMeshProUGUI textButtonIniciar;

        [SerializeField]
        private Button buttonIniciar;

        private ClsSeguridad refSeguridad;
        
        private string[] cmd;

        private string correoMono;

        private string nombreMono;

        private string instituMono;

        private string passwordMonoUser;
        #endregion

        #region monoBehaviour

        private void Awake()
        {
            refSeguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
        }

        // Use this for initialization
        private void Start()
        {
            refSeguridad.DlResLoginAula = mtdCallbackLogin;
            textUsuarioPlaceHolder.text = DiccionarioIdiomas._instance.Traducir("TextUsuario") + "...";
            textPasswordPlaceHolder.text = DiccionarioIdiomas._instance.Traducir("TextContraseña") + "...";

            if (refSeguridad.modoMonoUsuario)
            {
#if UNITY_STANDALONE
                // Obtenemos los argumentos enviados a la aplicación de escritorio.
                cmd = Environment.CommandLine.Split(',');

                try
                {
                    if (cmd.Length > 5)
                    {
                        nombreMono = cmd[3];
                        instituMono = cmd[4];
                        correoMono = cmd[6];
                        passwordMonoUser = cmd[7];
                    }
                }
                catch
                {
                    BoxMessageManager._instance.MtdCreateBoxMessageInfo("esto es lo que hay dentro de la cmd " + cmd, "ACEPTAR");
                }

#elif UNITY_ANDROID || UNITY_IPHONE
                try
                {
                    AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
                    AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

                    if (currentActivity != null)
                    {
                        AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");

                        if (intent != null)
                        {
                            nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                            instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                            correoMono = safeCallStringMethod(intent, "getStringExtra", "correo");
                            passwordMonoUser = safeCallStringMethod(intent, "getStringExtra", "password");
                        }
                    }
				}
                catch (Exception e)
                {
					Debug.Log(e.ToString());
				}
#endif
                // inputPassword.text = correoMono;
            }
        }

        // Update is called once per frame
        private void Update()
        {
#if UNITY_EDITOR || UNITY_STANDALONE||UNITY_ANDROID || UNITY_IPHONE

            if (refSeguridad.modoMonoUsuario)
            {
                inputUsuario.text = correoMono;

                if (refSeguridad.ModoAula)
                    inputPassword.text = passwordMonoUser;
            }
#endif
        }

        #endregion

        #region private methods

        /// <summary>
        /// metodo que se agrega al delegado de seguridad para saber la respuesta del la operacion loguin
        /// </summary>
        /// <param name="op"></param>
        private void mtdCallbackLogin(int op)
        {
            if (op == 1)
                Mostrar(false);
            else
            {
                if (op == 0)
                {
                    textButtonIniciar.text = DiccionarioIdiomas._instance.Traducir("TextIngresar");
                    buttonIniciar.enabled = true;
                }
            }
        }

        #endregion

        #region public methods

        public void LimpiarCampoUsuario()
        {
            textUsuarioPlaceHolder.text = "";
        }
        
        public void LimpiarCampoContrasenia()
        {
            textPasswordPlaceHolder.text = "";
        }
        
        /// <summary>
        /// metodo que verifica campos de loguin y pasa los datos a seguridad
        /// </summary>
        public void MtdBtnIniciarAplicacion()
        {
            if (inputUsuario.text != "" && inputPassword.text != "")
            {
                Debug.Log("entre a llamar aula loginRequest");
                refSeguridad.mtdLoguinAula(inputUsuario.text, inputPassword.text);
                textButtonIniciar.text = DiccionarioIdiomas._instance.Traducir("TextCargando");
                buttonIniciar.enabled = false;
            }
            else
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeCamposNecesarios"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"));
        }
        #endregion

        public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
        {
            if (args == null)
                args = new object[] { null };

            IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
            jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);

            try
            {
                IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);

                if (IntPtr.Zero != returnValue)
                {
                    var val = AndroidJNI.GetStringUTFChars(returnValue);
                    AndroidJNI.DeleteLocalRef(returnValue);
                    return val;
                }
            }
            finally
            {
                AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
            }

            return null;
        }
    }
}