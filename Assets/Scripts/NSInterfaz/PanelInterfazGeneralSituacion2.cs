﻿using System.Collections;
using System.Collections.Generic;
using NSActiveZones;
using NSBoxMessage;
using NSSituacion2;
using NSTraduccionIdiomas;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZonaActual = NSSituacion2.ZonaActual;
using NSMenuSituationSelection;

namespace NSInterfaz
{
    public class PanelInterfazGeneralSituacion2 : AbstractSingletonPanelUIAnimation<PanelInterfazGeneralSituacion2>
    {
        [SerializeField] private ControladorSituacion2 refControladorSituacion1;

        [SerializeField] private ActiveZonesController refActiveZonesController;

        public void OnButtonHerramientas()
        {
            if (refControladorSituacion1._etapaSituacionActual == ZonaActual.Null || refControladorSituacion1._etapaSituacionActual == ZonaActual.VestirAvatar || refControladorSituacion1._etapaSituacionActual == ZonaActual.ZonaSeguridad)
                BoxMessageManager._instance.MtdCreateBoxMessageInfo(DiccionarioIdiomas._instance.Traducir("mensajeNoPuedeUsarHerramientaAqui") , DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), CancelDecision);
            else
                PanelInterfazCajaHerramientas._instance.Mostrar();
        }

        public void OnButtonReiniciarSituacion()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("TextPreguntaReiniciar"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), RestartSituationConfirm, CancelDecision);
        }

        public void OnButtonReturnBackSituation()
        {
            BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("mensajeAbandonarPractica"), DiccionarioIdiomas._instance.Traducir("TextCancelar"), DiccionarioIdiomas._instance.Traducir("TextAceptarMayusculas"), ConfirmReturnBackSituation, CancelDecision);
        }

        public void RestartSituationConfirm()
        {
            GameObject.FindGameObjectWithTag("canvasMenuIni").GetComponent<SeleccionSituacion>().activarBienvenida();
        }

        private void ConfirmReturnBackSituation()
        {
            SceneManager.LoadScene(0);
        }

        private void CancelDecision()
        {
            refActiveZonesController.ActiveAllZones();
        }
    }
}