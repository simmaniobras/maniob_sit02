﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NSEvaluacion;
using NSSeguridad;

namespace NSInterfaz
{
    public class PanelBarraUsuario : MonoBehaviour
    {
        #region members

        [SerializeField]
        private Animator animatorBarraInformacionUsuario;

        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField]
        private TextMeshProUGUI textTiempoSesion;

        [SerializeField]
        private TextMeshProUGUI textCantidadIntentos;

        [SerializeField]
        private TextMeshProUGUI textNombreUsuario;
        #endregion

        #region monoBehaviour

        void Awake()
        {
            refControladorDatosSesion._listenerCantidadIntentos += ActualizarValorCantidadIntentos;
            refControladorDatosSesion._listenerTiempoSituacion += ActualizarValorTiempoSesion;
            ActualizarValorNombreUsuario();
        }

        private void OnEnable()
        {
            refControladorDatosSesion.IniciarNuevaSesionSituacion();
        }
        #endregion

        #region public methods

        public void OnButtonDesplegarContraerMenu()
        {
            animatorBarraInformacionUsuario.SetBool("desplegar", !animatorBarraInformacionUsuario.GetBool("desplegar"));
        }

        public void ActualizarValorTiempoSesion(float argTiempoSesionActual)
        {
            var tmpSegundos = Mathf.Floor(argTiempoSesionActual % 60);
            var tmpMinutos = Mathf.Floor(argTiempoSesionActual / 60);
            textTiempoSesion.text = ((tmpMinutos < 10f) ? "0" + tmpMinutos : tmpMinutos.ToString()) + ":" + (tmpSegundos < 10f ? "0" + tmpSegundos : tmpSegundos.ToString());
        }

        public void ActualizarValorCantidadIntentos(byte argCantidadIntentosActuales)
        {
            if (textCantidadIntentos)
                textCantidadIntentos.text = argCantidadIntentosActuales.ToString();
        }

        public void ActualizarValorNombreUsuario()
        {

            var tmpDatosConexion = ClsSeguridad._instance.GetDatosSesion();          
            textNombreUsuario.text = tmpDatosConexion[0];
            
        }
        #endregion
    }
}
