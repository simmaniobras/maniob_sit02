﻿using NSAvancedUI;
using NSBoxMessage;
using NSTraduccionIdiomas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSInterfaz
{
    public class PanelInterfazWearAvatar : AbstractSingletonPanelUIAnimation<PanelInterfazWearAvatar>
    {        
        //private  PanelSi

        public void OnButtonClosePanel()
        {
            ExecuteClosePanel();
            /*BoxMessageManager._instance.MtdCreateBoxMessageDecision(DiccionarioIdiomas._instance.Traducir("textoMensajeCerrarPanelAvatar", "Desea cerrar el panel de vestir el avatar"),
                DiccionarioIdiomas._instance.Traducir("botonCancelarCerrarPanelAvatar", "Cancelar"),
                DiccionarioIdiomas._instance.Traducir("botonAceptarCerrarPanelAvatar", "Aceptar") , ExecuteClosePanel);*/
        }

        public void ExecuteClosePanel()
        {
            Mostrar(false);
            //PanelInterfazGeneralSituacion1._instance.Mostrar();
        }
    } 
}
